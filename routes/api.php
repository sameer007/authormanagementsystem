<?php

use App\Http\Controllers\ArtistController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MusicController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {

    Route::post('/login', [AuthController::class, 'login']);

    Route::post('/register', [AuthController::class, 'register']);


});
Route::group(['prefix' => 'home',  'middleware' => 'auth:sanctum'], function () {
    Route::group(['prefix' => 'user'], function () {

        Route::post('/addUser', [UserController::class, 'addUser']);
        Route::put('/editUser/{id}', [UserController::class, 'editUser']);
        Route::delete('/deleteUser/{id}', [UserController::class, 'deleteUser']);
        Route::get('/userList', [UserController::class, 'getUserList']);
        Route::get('/check-user/{id}', [UserController::class, 'getUserById']);

    });
    Route::group(['prefix' => 'artist'], function () {

        Route::post('/addArtist', [ArtistController::class, 'addNewArtist']);
        Route::post('/importArtists', [ArtistController::class, 'addArtistsFromCSV']);

        Route::put('/editArtist/{id}', [ArtistController::class, 'editArtistInfo']);
        Route::delete('/deleteArtist/{id}', [ArtistController::class, 'deleteArtist']);
        Route::get('/singleArtist/{id}', [ArtistController::class, 'getArtistById']);
        Route::get('/artistList', [ArtistController::class, 'getArtistList']);
        Route::get('/{artistId}/musicList', [ArtistController::class, 'getMusicListByArtistId']);


    });
    Route::group(['prefix' => 'music'], function () {

        Route::post('/addMusic/{artistId}', [MusicController::class, 'addNewMusic']);
        Route::put('/editMusic/{id}', [MusicController::class, 'editMusicInfo']);
        Route::delete('/deleteMusic/{id}', [MusicController::class, 'deleteMusic']);
        Route::get('/singleMusic/{id}', [MusicController::class, 'getMusicById']);
        Route::get('/musicList', [MusicController::class, 'getMusicList']);

    });
    Route::get('/dashboard/{id}', [UserController::class, 'getLoggedInUserData']);
});
Route::fallback(function () {
    return response()->json(['message' => 'Not Found'], 404);
});