<?php

use App\Http\Controllers\ArtistWebController;
use App\Http\Controllers\AuthWebController;
use App\Http\Controllers\DashboardWebController;
use App\Http\Controllers\MusicWebController;
use App\Http\Controllers\UserWebController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    return redirect('auth/login');
});
Route::group(['prefix' => 'auth'],function () {
    Route::get('/login', [AuthWebController::class,'showLoginPage'])->name('login');
    //Route::get('/logout', [UserController::class,'logout'])->name('logout');
    Route::get('/register', [AuthWebController::class,'showRegisterPage'])->name('register');
    //Route::post('/login', [AuthWebController::class, 'login']);
    Route::post('/register', [AuthWebController::class, 'register']);
    Route::get('/logout', [AuthWebController::class,'logout'])->name('logout');

});

Route::group(['prefix' => 'home'],function () {

    Route::group(['prefix' => 'user'], function () {

        Route::get('/addUser', [UserWebController::class, 'showCreateNewUserPage']);
        Route::get('/editUser/{id}', [UserWebController::class, 'showEditUserPage']);
        Route::get('/userList', [UserWebController::class, 'showUserList']);


    });

    Route::group(['prefix' => 'artist'], function () {

        Route::get('/addArtist', [ArtistWebController::class, 'showAddArtistPage']);
        Route::get('/editArtist/{id}', [ArtistWebController::class, 'showEditArtistPage']);
        Route::get('/artistList', [ArtistWebController::class, 'showArtistListPage']);
        Route::get('/{id}/musicList', [ArtistWebController::class, 'showArtistMusicPage']);

        Route::get('/importArtists', [ArtistWebController::class, 'showImportArtistsPage']);

    });

    Route::group(['prefix' => 'music'], function () {

        Route::get('/addMusic/{id}', [MusicWebController::class, 'showAddNewMusicPage']);
        Route::get('/editMusic/{id}', [MusicWebController::class, 'showEditMusicPage']);
        //Route::get('/deleteMusic/{id}', [MusicWebController::class, 'deleteMusic']);
        Route::get('/musicList', [MusicWebController::class, 'showAllMusicListPage']);

    });
    
    Route::get('/dashboard', [DashboardWebController::class,'showDashboard'])->name('showDashboard');
    
});
Route::fallback(function () {
    return redirect('home/dashboard')->with([
        'error' => 'Invalid route detected'
    ]);
});
