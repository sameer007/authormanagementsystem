@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> User</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->

            @php
            @endphp
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> <span id="title"></span></h4>
                        <form id="editUserForm" action="/home/music/editUser" METHOD="PUT">
                            <div class="box-body"></div>
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="firstName" autofocus="" name="first_name" placeholder="" type="text" value="" autocomplete="off" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Last Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="lastName" autofocus="" name="last_name" placeholder="" type="text" value="" autocomplete="off" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6">
                                <label>DOB</label>
                                <small class="req"> *</small>
                                <input type="date" id="dob" name="dob" class="form-control" placeholder="DOB" autofocus>
                                <span class="text-danger"></span>
                            </div>

                            <div class="form-group col-md-6">
                                <label>Phone no</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="phone" autofocus="" name="phone" placeholder="" type="number" value="" autocomplete="off" />
                                <span class="text-danger"></span>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-6">
                                <div class="radio">
                                    <label>Gender:</label>

                                    <label>
                                        <input type="radio" name="gender" id="genderm" value="m">
                                        Male
                                    </label>
                                    <label>
                                        <input type="radio" name="gender" id="genderf" value="f">
                                        Female
                                    </label>
                                    <label>
                                        <input type="radio" name="gender" id="gendero" value="o">
                                        Other
                                    </label>
                                </div>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input class="form-control" id="address" name="address" placeholder="" type="text" value="" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="update-music">Update User</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        let startYear = 1800;
        let endYear = new Date().getFullYear();
        for (i = endYear; i > startYear; i--) {
            $('#firstReleaseYear').append($('<option/>').val(i).html(i));
        }
    </script>
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', bearerToken);
        console.log('user', user);
        const userData = JSON.parse(user);


        //+ userData.id
        var currentUrlArr = window.location.pathname.split("/");

        var userIdFromPath = currentUrlArr[currentUrlArr.length - 1];
        var apiUrl = "http://localhost:8000/api/home/user/check-user/" + userIdFromPath;

        console.log('apiUrl', apiUrl);


        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };
        let genderElId;
        fetch(apiUrl, {
                method: 'GET',
                headers: headers,
            })
            .then(response => {
                return response.json();
            })
            .then(data => {
                //console.log(musicData.success);
                //var allArtists = JSON.stringify(musicData.allArtists);
                console.log('UserData', data.UserData);
                var userData = data.UserData;

                console.log(userData);
                genderElId = '#gender' + userData.gender;

                var firstName = $('#firstName');
                var lastName = $('#lastName');
                var dob = $('#dob');
                var phone = $('#phone');
                var gender = $(genderElId);
                var address = $('#address');
                var genre = $('#genre');

                $("#title").html('Edit User Info of ' + userData.first_name + ' ' + userData.last_name);

                var dateDob = userData.dob.split(' ')[0];
                // The first part contains the date 'YYYY-MM-DD'

                firstName.val(userData.first_name);
                lastName.val(userData.last_name);
                dob.val(dateDob);
                phone.val(userData.phone);
                gender.attr('checked', 'checked');
                address.val(userData.address);
                genre.val(userData.genre);
                
            });
        // Make a PUT request to send form data

        document.getElementById("editUserForm").addEventListener("submit", function(event) {
            event.preventDefault(); // Prevent the default form submission behavior

            // Get form data
            //const formData = new FormData(event.target);
            //const formData = new FormData(this);
            var firstName = $('#firstName');
            var lastName = $('#lastName');
            var dob = $('#dob');
            var phone = $('#phone');
            var gender = $(genderElId);
            var address = $('#address');
            var genre = $('#genre');

            const formData = {
                'first_name': firstName.val(),
                'last_name': lastName.val(),
                'phone': phone.val(),
                'gender': gender.val(),
                'dob': dob.val(),
                'address': address.val(),

            }
            console.log('formData>>', formData);

            // Make a POST request to send form data

            // Define the API endpoint
            var updateUserApiUrl = "http://localhost:8000/api" + window.location.pathname;

            //var token = localStorage.getItem('token').toString();
            //var bearerToken = 'Bearer ' + token;
            console.log('updateUserApiUrl', updateUserApiUrl);
            console.log('headers', headers);

            fetch(updateUserApiUrl, {
                    method: "PUT",
                    body: JSON.stringify(formData),
                    headers: headers,
                })
                .then((response) => response.json())
                .then((data) => {
                    // Handle the API response (e.g., show a success message)
                    //var userData = JSON.parse(data);
                    console.log('userData>>', data);
                    if (data) {
                        console.log('updateUserData>>', data.updatedUserData[0].id);
                        if (data.updatedUserData[0]) {

                            var updatedUserData = data.updatedUserData[0];
                            $("#title").html('Edit User Info of ' + updatedUserData.first_name + ' ' + updatedUserData.last_name);
                            if (data.success) {
                                notify('success', data.success);
                            } else if (data.error) {
                                notify('error', data.error);
                            }

                        }

                    }

                })
                .catch((error) => {
                    // Handle errors (e.g., show an error message)
                    console.error("Error:", error);
                });
        });
    </script>
    <script>

    </script>
    @endsection