<aside>
    @php

    $currentPage = substr(strrchr(url()->current(),"/"),1);
    $profileUrl = url('https://cloud.xm-cdn.com/static/xm/common/logos/XMLogo-2021_homepage.svg');


    @endphp

    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered"><a href="profile.html"><img src="{{$profileUrl}}" class="img-square" width="80"></a></p>
            <h5 class="centered" id="loggedInUser">User</h5>
            <li class="mt">
                <a class="{{ ($currentPage == 'home/dashboard') ? 'active' : ''  }}" href="{{url('home/dashboard')}}">
                    <i class="fa fa-table"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" class="{{ Request::is('home/artist/*') ? 'active' : ''  }}" href="{{url('home/artist')}}">
                    <i class="fa fa-book"></i>
                    <span>Artist</span>
                </a>
                <ul class="sub">
                    <li class="{{ Request::is('home/artist/addArtist*') ? 'active' : ''  }}"><a href="{{url('home/artist/addArtist')}}">Add Artist</a></li>
                    <li class="{{ Request::is('home/artist/artistList*') ? 'active' : ''  }}"><a href="{{url('home/artist/artistList')}}">Artist List</a></li>
                    <li class="{{ Request::is('home/artist/importArtists*') ? 'active' : ''  }}"><a href="{{url('home/artist/importArtists')}}">Import Artists</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" class="{{ Request::is('home/music/*') ? 'active' : ''  }}" href="{{url('home/music')}}">
                    <i class="fa fa-book"></i>
                    <span>Music</span>
                </a>
                <ul class="sub">
                    <li class="{{ Request::is('home/music/allMusicList') ? 'active' : ''  }}"><a href="{{url('home/music/musicList')}}">All Music List</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" class="{{ Request::is('home/user/*') ? 'active' : ''  }}" href="{{url('home/user')}}">
                    <i class="fa fa-book"></i>
                    <span>Users</span>
                </a>
                <ul class="sub">
                    <li class="{{ Request::is('home/user/addUser') ? 'active' : ''  }}"><a href="{{url('home/user/addUser')}}">Add User</a></li>
                    <li class="{{ Request::is('home/user/userList') ? 'active' : ''  }}"><a href="{{url('home/user/userList')}}">User List</a></li>
                </ul>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>