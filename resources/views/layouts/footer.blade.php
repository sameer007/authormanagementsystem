<footer>
  <div class="text-center">

  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="{{url('lib/jquery/jquery.min.js')}}"></script>
  <script src="{{url('lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{url('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
  <script src="{{url('lib/jquery.ui.touch-punch.min.js')}}"></script>
  <script class="include" type="text/javascript" src="{{url('lib/jquery.dcjqaccordion.2.7.js')}}"></script>
  <script src="{{url('lib/jquery.scrollTo.min.js')}}"></script>
  <script src="{{url('lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="{{url('lib/common-scripts.js')}}"></script>
  <!--script for this page-->

  @yield('customJs')
  
  <script>
    document.getElementById("logout").addEventListener("click", function(event) {
      event.preventDefault();
      logUserOut = confirm('Are you sure you want to logout?');

      if(logUserOut){
        localStorage.clear();

        notify('success','Logged Out Successfully');

        setTimeout(function(){
          window.location.href = '/auth/login';

        },1500);
      }

    });
  </script>
</footer>
<!--footer end-->
</section>