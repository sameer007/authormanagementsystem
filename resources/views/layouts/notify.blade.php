<div class="notification alert alert-danger hidden" id="errorNotify">
    <ul>
        <li></li>
    </ul>
</div>


<div class="notification alert alert-success hidden " id="successNotify">
    <ul>
        <li></li>
    </ul>
</div>

<div class="notification alert alert-info hidden" id="infoNotify">
    <ul>
        <li></li>
    </ul>
</div>


<div class="notification alert alert-warning hidden" id="warningNotify">
    <ul>
        <li></li>
    </ul>
</div>