@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Music</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->

            @php
            @endphp
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> <span id="title"></span></h4>
                        <form id="addMusicForm" action="/home/music/addMusic" METHOD="POST">

                            <div class="box-body"></div>
                            <div class="form-group col-md-6">
                                <label>Title</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="musicTitle" autofocus="" name="name" placeholder="" type="text" value="" autocomplete="off" required/>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Album Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="albumName" autofocus="" name="album_name" placeholder="" type="text" value="" autocomplete="off" required/>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Genre</label>
                                <small class="req"> *</small>
                                <select class="form-control" id="genre" name="genre" required>
                                    <option value=""></option>
                                    <option value="rnb">RnB</option>
                                    <option value="country">Country</option>
                                    <option value="classic">Classic</option>
                                    <option value="rock">Rock</option>
                                    <option value="jazz">Jazz</option>
                                </select>
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="update-music">Add Music</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        let startYear = 1800;
        let endYear = new Date().getFullYear();
        for (i = endYear; i > startYear; i--) {
            $('#firstReleaseYear').append($('<option/>').val(i).html(i));
        }
    </script>
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', bearerToken);
        console.log('user', user);
        const userData = JSON.parse(user);
        
        var currentUrlArr = window.location.pathname.split("/");

        var artistIdFromPath = currentUrlArr[currentUrlArr.length - 1];
        var artistApiUrl = "http://localhost:8000/api/home/artist/singleArtist/" + artistIdFromPath;

        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };
        
        fetch(artistApiUrl, {
                method: 'GET',
                headers: headers,
            })
            .then(response => {
                return response.json();
            })
            .then(data => {
                //console.log(artistData.success);
                //var allArtists = JSON.stringify(artistData.allArtists);
                console.log('artistData', data.artistData);
                var artistData = data.artistData;
                $("#title").html('Add New music of Artist: ' + artistData.name);
                

            });
        // Make a PUT request to send form data

        document.getElementById("addMusicForm").addEventListener("submit", function(event) {
            event.preventDefault(); // Prevent the default form submission behavior

            // Get form data
            //const formData = new FormData(event.target);
            //const formData = new FormData(this);
            var musicTitle = $('#musicTitle');
            var albumName = $('#albumName');
            var genre = $('#genre');
            
            const formData = {
                'title' : musicTitle.val(),
                'album_name': albumName.val(),
                'genre': genre.val()
            }
            console.log('formData>>', formData);

            // Make a POST request to send form data

            // Define the API endpoint
            var addMusicApiUrl = "http://localhost:8000/api"+ window.location.pathname;
            
            console.log('addMusicApiUrl', addMusicApiUrl);
            console.log('headers', headers);

            fetch(addMusicApiUrl, {
                    method: "POST",
                    body: JSON.stringify(formData),
                    headers: headers,
                })
                .then((response) => response.json())
                .then((addMusicData) => {
                    // Handle the API response (e.g., show a success message)
                    //var userData = JSON.parse(data);
                    console.log('addMusicData>>', addMusicData);
                    if(addMusicData.success){
                        notify('success',addMusicData.success);
                    }else if (addMusicData.error){
                        notify('error',addMusicData.error);
                    }
                })
                .catch((error) => {
                    // Handle errors (e.g., show an error message)
                    console.error("Error:", error);
                });
        });
    </script>
    <script>

    </script>
    @endsection