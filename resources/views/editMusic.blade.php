@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Music</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->

            @php
            @endphp
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> <span id="title"></span></h4>
                        <form id="editMusicForm" action="/home/music/editMusic" METHOD="PUT">

                            <div class="box-body"></div>
                            <div class="form-group col-md-6">
                                <label>Title</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="musicTitle" autofocus="" name="name" placeholder="" type="text" value="" autocomplete="off" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Album Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="albumName" autofocus="" name="album_name" placeholder="" type="text" value="" autocomplete="off" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Genre</label>
                                <small class="req"> *</small>
                                <select class="form-control" id="genre" name="genre">
                                    <option value=""></option>
                                    <option value="rnb">RnB</option>
                                    <option value="country">Country</option>
                                    <option value="classic">Classic</option>
                                    <option value="rock">Rock</option>
                                    <option value="jazz">Jazz</option>
                                </select>
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="update-music">Update Music</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        let startYear = 1800;
        let endYear = new Date().getFullYear();
        for (i = endYear; i > startYear; i--) {
            $('#firstReleaseYear').append($('<option/>').val(i).html(i));
        }
    </script>
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', bearerToken);
        console.log('user', user);
        const userData = JSON.parse(user);


        //+ userData.id
        var currentUrlArr = window.location.pathname.split("/");

        var musicIdFromPath = currentUrlArr[currentUrlArr.length - 1];
        var apiUrl = "http://localhost:8000/api/home/music/singleMusic/" + musicIdFromPath;

        console.log('apiUrl', apiUrl);


        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };
        let genderElId;
        fetch(apiUrl, {
                method: 'GET',
                headers: headers,
            })
            .then(response => {
                return response.json();
            })
            .then(data => {
                
                console.log('musicData', data.musicData);
                var musicData = data.musicData;

                console.log(musicData);
                var musicTitle = $('#musicTitle');
                var albumName = $('#albumName');
                var genre = $('#genre');

                $("#title").html('Edit Music Info of song :  `' + musicData.title + '` from album `' + musicData.album_name + '`');

                musicTitle.val(musicData.title);
                albumName.val(musicData.name);
                genre.val(musicData.genre);
                
            });
        // Make a PUT request to send form data

        document.getElementById("editMusicForm").addEventListener("submit", function(event) {
            event.preventDefault(); // Prevent the default form submission behavior

            // Get form data
            
            var musicTitle = $('#musicTitle');
            var albumName = $('#albumName');
            var genre = $('#genre');

            const formData = {
                'title': musicTitle.val(),
                'album_name': albumName.val(),
                'genre': genre.val()
            }
            console.log('formData>>', formData);

            // Make a POST request to send form data

            // Define the API endpoint
            var updateMusicApiUrl = "http://localhost:8000/api" + window.location.pathname;
            
            console.log('updateMusicApiUrl', updateMusicApiUrl);
            console.log('headers', headers);

            fetch(updateMusicApiUrl, {
                    method: "PUT",
                    body: JSON.stringify(formData),
                    headers: headers,
                })
                .then((response) => response.json())
                .then((data) => {
                    // Handle the API response (e.g., show a success message)
                    //var userData = JSON.parse(data);
                    console.log('updateMusicApiUrl>>', data);
                    if (data) {
                        console.log('updatedFMusicData>>', data.updatedMusicData[0].id);
                        if (data.updatedMusicData[0]) {

                            var updatedMusicData = data.updatedMusicData[0];
                            $("#title").html('Edit Music Info of song :  `' + updatedMusicData.title + '` from album `' + updatedMusicData.album_name + '`');

                            if (updatedMusicData.success) {
                                notify('success', updatedMusicData.success);
                            } else if (updatedMusicData.error) {
                                notify('error', updatedMusicData.error);
                            }

                        }

                    }

                })
                .catch((error) => {
                    // Handle errors (e.g., show an error message)
                    console.error("Error:", error);
                });
        });
    </script>
    <script>

    </script>
    @endsection