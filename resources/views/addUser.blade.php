@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> User</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->

            @php
            @endphp
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> <span id="title">Add User</span></h4>
                        <form id="addUserForm" action="/home/music/addUser" METHOD="POST">
                            <div class="box-body"></div>
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <small class="req"> *</small>
                                <input type="email" name="email" id="email" class="form-control" placeholder="User Email" autofocus required>

                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <label>Password</label>
                                <small class="req"> *</small>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>

                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Re-type Password</label>
                                <small class="req"> *</small>
                                <input type="password" name="repassword" id="repassword" class="form-control" placeholder="Re-type Password" required>
                                
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="firstName" autofocus="" name="first_name" placeholder="" type="text" value="" autocomplete="off" required/>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Last Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="lastName" autofocus="" name="last_name" placeholder="" type="text" value="" autocomplete="off" required/>
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group col-md-6">
                                <label>DOB</label>
                                <small class="req"> *</small>
                                <input type="date" id="dob" name="dob" class="form-control" placeholder="DOB" autofocus required>
                                <span class="text-danger"></span>
                            </div>

                            <div class="form-group col-md-6">
                                <label>Phone no</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="phone" autofocus="" name="phone" placeholder="" type="number" value="" autocomplete="off" required/>
                                <span class="text-danger"></span>
                            </div>

                            <div class="clearfix"></div>
                            
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input class="form-control" id="address" name="address" placeholder="" type="text" value="" required/>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Gender:</label>

                                <div class="radio">

                                    <label>
                                        <input type="radio" name="gender" id="genderm" value="m" checked>
                                        Male
                                    </label>
                                    <label>
                                        <input type="radio" name="gender" id="genderf" value="f">
                                        Female
                                    </label>
                                    <label>
                                        <input type="radio" name="gender" id="gendero" value="o">
                                        Other
                                    </label>
                                </div>
                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="add-user">Add User</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        let startYear = 1800;
        let endYear = new Date().getFullYear();
        for (i = endYear; i > startYear; i--) {
            $('#firstReleaseYear').append($('<option/>').val(i).html(i));
        }
    </script>
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', bearerToken);
        console.log('user', user);
        const userData = JSON.parse(user);
        
        var currentUrlArr = window.location.pathname.split("/");

        var userIdFromPath = currentUrlArr[currentUrlArr.length - 1];
        var userApiUrl = "http://localhost:8000/api/home/user/check-user/" + userIdFromPath;

        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };
        
        
        // Make a PUT request to send form data

        document.getElementById("addUserForm").addEventListener("submit", function(event) {
            event.preventDefault(); // Prevent the default form submission behavior

            // Get form data
            // const formData = new FormData(event.target);
            // const formData = new FormData(this);

            var firstName = $('#firstName');
            var lastName = $('#lastName');
            var dob = $('#dob');
            var phone = $('#phone');
            var gender = $('input[name="gender"]:checked');
            var address = $('#address');
            var genre = $('#genre');

            var email = $('#email');
            var password = $('#password');
            var repassword = $('#repassword');

            

            const formData = {
                'first_name': firstName.val(),
                'last_name': lastName.val(),
                'phone': phone.val(),
                'gender': gender.val(),
                'dob': dob.val(),
                'address': address.val(),
                'email': email.val(),
                'password': address.val(),
                'repassword': repassword.val(),

            }
            console.log('formData>>', formData);

            // Make a POST request to send form data

            // Define the API endpoint

            var addUserApiUrl = "http://localhost:8000/api"+ window.location.pathname;
            
            console.log('addUserApiUrl', addUserApiUrl);
            console.log('headers', headers);

            fetch(addUserApiUrl, {
                    method: "POST",
                    body: JSON.stringify(formData),
                    headers: headers,
                })
                .then((response) => response.json())
                .then((addedUserData) => {
                    // Handle the API response (e.g., show a success message)
                    //var userData = JSON.parse(data);
                    console.log('addedUserData>>', addedUserData);

                    if(addedUserData.success){
                        notify('success',addedUserData.success);
                    }else if (addedUserData.error){
                        notify('error',addedUserData.error);
                    }
                    // if (data.updatedArtistData[0]) {
                    //     var updatedArtistData = data.updatedArtistData[0];
                    //     $("#title").html('Edit Artist Info of ' + updatedArtistData.name);

                    // }

                })
                .catch((error) => {
                    // Handle errors (e.g., show an error message)
                    console.error("Error:", error);
                });
        });
    </script>
    <script>

    </script>
    @endsection