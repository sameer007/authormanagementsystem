@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <h3><i class="fa fa-angle-right"></i>Dashboard</h3>
            @include('layouts.notify')
            <div class="row mt">
                <div class="col-lg-12">
                    @php
                    
                    @endphp
                    <p>Welcome to Artist Managenement System</p>
                    
                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    
    
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', token);
        console.log('user', user);
        const userData = JSON.parse(user);
        //+ userData.id
        var apiUrl = "http://localhost:8000/api/home/dashboard/" + userData.id;

        console.log('apiUrl', apiUrl);

        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };

        fetch(apiUrl, {
                method: 'GET',
                headers: headers,
            })
            .then(response => {
                return response.json();
            })
            .then(dashboardData => {
                var userData = JSON.stringify(dashboardData.user) ;
                localStorage.setItem('user',userData);
                //echo success msg here

                if (userData) {
                    notify('success', 'Welcome to AMS dashboard : ' + dashboardData.user.first_name);
                } else{
                    notify('error', 'Something went wrong while logging in');
                }
                
            });
        // Make a POST request to send form data
    </script>
    @endsection