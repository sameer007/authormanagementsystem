@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Artist</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->

            @php
            @endphp
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> <span id="title">Import Artists with CSV</span></h4>
                        <form id="importArtistForm" action="/home/artist/importArtists" enctype="multipart/form-data" METHOD="POST">

                            <div class="box-body"></div>

                            <div class="form-group col-md-6">
                                <label>Import Artists CSV file</label>
                                <small class="req"> *</small>
                                <div class="col-md-4">
                                    <input id="csvFile" type="file" name="artistsCSV" accept=".csv" class="default" />
                                </div>
                                <span class="text-danger"></span>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="import-artist">Import Artists</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        let startYear = 1800;
        let endYear = new Date().getFullYear();
        for (i = endYear; i > startYear; i--) {
            $('#firstReleaseYear').append($('<option/>').val(i).html(i));
        }
    </script>
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', bearerToken);
        console.log('user', user);
        const userData = JSON.parse(user);

        const headers = {
            'Authorization': bearerToken, // Example authorization header
            'Accept': 'application/json',
        };

        // Add a listener on your input
        // It will be triggered when a file will be selected

        // Make a PUT request to send form data

        document.getElementById("importArtistForm").addEventListener("submit", function(event) {
            event.preventDefault(); // Prevent the default form submission behavior

            var csvFile = document.getElementById('csvFile').files[0];
            console.log('csvFile>>', csvFile);
            const formData = new FormData();
            formData.append('artistsCSV', csvFile);
            console.log('formData>>', formData);

            // Make a POST request to send form data

            // Define the API endpoint

            var importArtistsApiUrl = "http://localhost:8000/api" + window.location.pathname;
            
            console.log('importArtistsApiUrl', importArtistsApiUrl);
            console.log('headers', headers);

            fetch(importArtistsApiUrl, {
                    method: "POST",
                    
                    body: formData,
                    headers: headers,
                })
                .then((response) => response.json())
                .then((data) => {
                    // Handle the API response (e.g., show a success message)
                    console.log('artistsAdded>>', data);
                    if (data.success) {
                        notify('success', data.success);
                    } else if (data.error) {
                        notify('error', data.error);
                    }

                })
                .catch((error) => {
                    // Handle errors (e.g., show an error message)
                    console.error("Error:", error);
                });
        });
    </script>
    <script>

    </script>
    @endsection