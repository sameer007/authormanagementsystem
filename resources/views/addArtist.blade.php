@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Artist</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->

            @php
            @endphp
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> <span id="title"></span>Add Artist</h4>
                        <form id="addArtistForm" action="/home/artist/addArtist" METHOD="POST">

                            <div class="box-body"></div>
                            <div class="form-group col-md-6">
                                <label>Artist Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="artistName" autofocus="" name="name" placeholder="" type="text" value="" autocomplete="off" required/>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>DOB</label>
                                <small class="req"> *</small>
                                <input type="date" id="dob" name="dob" class="form-control" placeholder="DOB" max="return new Date().toISOString().split('T')[0]" autofocus required>
                                <span class="text-danger"></span>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-6">
                                <div class="radio">
                                    <label>Gender:</label>

                                    <label>
                                        <input type="radio" name="gender" id="genderm" value="m" checked>
                                        Male
                                    </label>
                                    <label>
                                        <input type="radio" name="gender" id="genderf" value="f">
                                        Female
                                    </label>
                                    <label>
                                        <input type="radio" name="gender" id="gendero" value="o">
                                        Other
                                    </label>
                                </div>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input class="form-control" id="address" name="address" placeholder="" type="text" value="" required/>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>First Release Year</label>
                                <small class="req"> *</small>
                                <select class="form-control" name="first_release_year" id="firstReleaseYear" required>
                                    <option value=""></option>
                                </select>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>No of Albums Relased</label>
                                <small class="req"> *</small>
                                <input type="number" id="noOfAlbumsReleased" name="no_of_albums_released" class="form-control" placeholder="No Of Albums Released" required>

                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="add-artist">Add Artist</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        let startYear = 1800;
        let endYear = new Date().getFullYear();
        for (i = endYear; i > startYear; i--) {
            $('#firstReleaseYear').append($('<option/>').val(i).html(i));
        }
    </script>
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', bearerToken);
        console.log('user', user);
        const userData = JSON.parse(user);
        
        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };
        

        // Make a PUT request to send form data

        document.getElementById("addArtistForm").addEventListener("submit", function(event) {
            event.preventDefault(); // Prevent the default form submission behavior

            // Get form data
            // const formData = new FormData(event.target);
            // const formData = new FormData(this);


            var artistName = $('#artistName');
            var dob = $('#dob');
            var gender = $('input[name="gender"]:checked');
            var address = $('#address');
            var firstReleaseYear = $('#firstReleaseYear');
            var noOfAlbumsReleased = $('#noOfAlbumsReleased');
            console.log(dob.val());

            const formData = {
                'name': artistName.val(),
                'dob': dob.val(),
                'gender': gender.val(),
                'address': address.val(),
                'first_release_year': firstReleaseYear.val(),
                'no_of_albums_released': Number(noOfAlbumsReleased.val()),
            }
            console.log('formData>>', formData);

            // Make a POST request to send form data

            // Define the API endpoint

            var addArtistApiUrl = "http://localhost:8000/api"+ window.location.pathname;
            
            console.log('addArtistApiUrl', addArtistApiUrl);
            console.log('headers', headers);

            fetch(addArtistApiUrl, {
                    method: "POST",
                    body: JSON.stringify(formData),
                    headers: headers,
                })
                .then((response) => response.json())
                .then((addedArtistData) => {
                    // Handle the API response 
                    console.log('addedUserData>>', addedArtistData);

                    if(addedArtistData.success){
                        notify('success',addedArtistData.success);
                    }else if (addedArtistData.error){
                        notify('error',addedArtistData.error);
                    }

                })
                .catch((error) => {
                    // Handle errors 
                    console.error("Error:", error);
                });
        });
    </script>
        <script type="text/javascript" src="{{url('js/formValidation.js')}}"></script>

    @endsection