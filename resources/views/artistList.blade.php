@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <h3><i class="fa fa-angle-right"></i>Artist</h3>
            @include('layouts.notify')
            <div class="row mt">
                <div class="col-lg-12">
                    @php
                    $sessionData = session()->all();


                    @endphp
                    <table class="table table-striped table-advance table-hover">

                        <h4><i class="fa fa-angle-right"></i> Artist List
                            <a class="btn btn-primary pull-right p-3" href="{{url('home/artist/addArtist')}}">Add Artist </a> &nbsp
                            <a class="btn btn-primary pull-right p-3" href="{{url('home/artist/importArtists')}}">Import Artist </a>
                        </h4>

                        <hr />
                        <thead>
                            <tr>
                                <th><i class="fa fa-bullhorn"></i>S.N</th>
                                <th><i class="fa fa-bullhorn"></i>Name</th>
                                <th class="hidden-phone"><i class="fa fa-question-circle"></i> DOB</th>
                                <th><i class="fa fa-bookmark"></i> GENDER</th>
                                <th><i class="fa fa-bookmark"></i> ADDRESS</th>
                                <th><i class="fa fa-edit"></i> Action</th>
                            </tr>
                        </thead>
                        <tbody id="artistListBody">

                        </tbody>
                    </table>
                    <div class="dataTables_paginate paging_bootstrap pagination">
                        <ul id="paginationUl">
                            <li class="prev disabled" id="prev"><a href="#">← Previous</a></li>
                            <!-- <li class="active"><a href="#">1</a></li>
                            <li"><a href="#">2</a></li> -->

                            <li class="next disabled" id="next"><a href="#">Next → </a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>


    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token');
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', bearerToken);
        console.log('user', user);
        const userData = JSON.parse(user);

        //+ userData.id
        const urlParams = new URLSearchParams(window.location.search);

        // Access individual query parameters
        const param2 = urlParams.get('param2');

        if (urlParams.has('page')) {
            // 'param1' exists in the URL
            var pageParam = urlParams.get('page'); // Gets the value of 'param1'
            var param = '?page='+ pageParam ;
        } else {
            var param = '' ;
        }
        var apiUrl = "http://localhost:8000/api" + window.location.pathname + param;

        //console.log('apiUrl', apiUrl);

        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };

        fetch(apiUrl, {
                method: 'GET',
                headers: headers,
            })
            .then(response => {
                return response.json();
            })
            .then(artistData => {
                //console.log(artistData.success);
                //var allArtists = JSON.stringify(artistData.allArtists);
                console.log('allArtists', artistData.allArtists);
                var allArtists = artistData.allArtists.data;

                var firstPageUrl = artistData.allArtists.first_age_url;
                var prevPageUrl = artistData.allArtists.prev_page_url;

                var nextPageUrl = artistData.allArtists.next_page_url;
                var lastPageUrl = artistData.allArtists.last_page_url;
                var pageUrlLinks = artistData.allArtists.links;

                var paginationUl = $('#paginationUl');
                var nextEl = $('#next');

                pageUrlLinks.forEach(function(linkItem, linkKey) {

                    if ((linkKey != 0) && (linkKey != (pageUrlLinks.length - 1))) {
                        var linkUrl = linkItem.url.replace("/api", "").replace('8000', '8080');

                        if(linkItem.label == pageParam){
                            var centerPageLink = $("<li class='active'><a href=" + linkUrl + ">" + linkItem.label + "</a></li>");

                        }else{
                            var centerPageLink = $("<li><a href=" + linkUrl + ">" + linkItem.label + "</a></li>");

                        }
                        console.log('centerPageLink>>', centerPageLink[0]);

                        
                        //paginationUl.append(centerPageLink);
                        centerPageLink.insertBefore(nextEl);
                    } else {
                        //var linkUrWithApi = linkItem.url;

                        //console.log('linkUrl>',linkUrl);

                        // if(linkKey == 0){
                        //     $('#prev a').attr('href',linkUrl);
                        // }else if(linkKey == (pageUrlLinks.length - 1)){
                        //     $('#next a').attr('href',nextPageUrl);

                        // }
                    }

                    console.log('linkItem>>', linkItem);
                });
                console.log('prevPageUrl', prevPageUrl);
                    console.log('nextPageUrl', nextPageUrl);

                if(prevPageUrl){
                    console.log('prevPageUrl', prevPageUrl);
                    
                    var lastPageLinkUrl = prevPageUrl.replace("/api", "").replace('8000', '8080');
                    $('#prev a').attr('href',lastPageLinkUrl);
                }
                if(nextPageUrl){
                    console.log('nextPageUrl', nextPageUrl);

                    var nextPageLinkUrl = nextPageUrl.replace("/api", "").replace('8000', '8080');
                    $('#next a').attr('href',nextPageLinkUrl);
                }
                var tableBody = $('#artistListBody');

                //var row = $('#artistListBody tr');
                //var tr_clone = $('#artistListBody tr').clone();


                //row = tr_clone;
                allArtists.forEach(function(item,key) {
                    var row = $("<tr></tr>");

                    if (item.gender == 'm') {
                        gender = 'Male';
                    } else if (item.gender == 'f') {
                        gender = 'Female';
                    } else {
                        gender = 'Others';
                    }
                    row.append("<td class='sn'>" + (key + 1) + "</td>");

                    row.append("<td class='name'>" + item.name + "</td>");
                    row.append("<td class='dob'>" + item.dob + "</td>");
                    row.append("<td class='gender'>" + gender + "</td>");
                    row.append("<td class='address'>" + item.address + "</td>");

                    var actionCell = $("<td class='action'></td>");
                    var musicListUrl = '/home/artist/' + item.id + '/musicList';
                    var editArtistUrl = '/home/artist/editArtist/' + item.id;
                    var deleteArtistUrl = '/home/artist/deleteArtist/' + item.id;

                    var addArtistMusicUrl = '/home/music/addMusic/' + item.id;


                    console.log('musicListUrl', musicListUrl);

                    actionCell.append("<a class='btn btn-success btn-md changeStatus' href=" + musicListUrl + ">Music List</a>");
                    actionCell.append("<a class='btn btn-primary btn-md changeStatus' href=" + addArtistMusicUrl + ">Add New Music</a>");

                    actionCell.append("<a class='btn btn-primary btn-md changeStatus' href=" + editArtistUrl + ">Edit</a>");
                    actionCell.append("<a class='btn btn-danger btn-md changeStatus deleteUrl' href='#' onClick='deleteArtist(" + item.id + ",`" + item.name + "`)'>Delete</a>");


                    row.append(actionCell);
                    tableBody.append(row);
                });
                //console.log(row[0])



            });
        // Make a DELETE request to send form data

        function deleteArtist(artistId, artistName) {
            var deleteApiEndpoint = "http://localhost:8000/api/home/artist/deleteArtist/" + artistId;
            console.log('delete>>', artistId);
            var userConfirmed = confirm("Are you sure you want to delete artist : `" + artistName + "` ?");

            if (userConfirmed) {
                fetch(deleteApiEndpoint, {
                        method: 'DELETE',
                        headers: headers
                    })
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Network response was not ok');
                        }
                        return response.json(); // Assuming you expect a JSON response
                    })
                    .then(data => {
                        // Handle the response data here
                        console.log('deleted>>', data);
                        if (data.success) {
                            if (data.success) {
                                notify('success', data.success);

                                setTimeout(function() {
                                    location.reload();
                                }, 3000);

                            } else if (data.error) {
                                notify('error', data.error);

                            }

                        }
                        // Optionally, you can update the UI to reflect the successful delete
                        // For example, you can remove the item from the page.
                    })
                    .catch(error => {
                        // Handle errors here
                        console.error('There was a problem with the fetch operation:', error);

                        // Optionally, you can display an error message or handle the error in other ways.
                    });
            }

        }
    </script>
    @endsection