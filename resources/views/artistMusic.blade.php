@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            @include('layouts.notify')
            <div class="row mt">
                <div class="col-lg-12">
                    @php
                    $sessionData = session()->all();


                    @endphp
                    <table class="table table-striped table-advance table-hover">

                        <h4><i class="fa fa-angle-right"></i> <span id="artistTitle">Music List</span> 
                        </h4>

                        <hr />
                        <thead>
                            <tr>
                                <th><i class="fa fa-bookmark"></i> S.N</th>
                                <th><i class="fa fa-bullhorn"></i>Title</th>
                                <th class="hidden-phone"><i class="fa fa-question-circle"></i> Album Name</th>
                                <th><i class="fa fa-bookmark"></i> Genre</th>
                                <th><i class="fa fa-edit"></i> Action</th>
                            </tr>
                        </thead>
                        <tbody id="musicListBody">

                        </tbody>
                    </table>


                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>

    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', token);
        console.log('user', user);
        const userData = JSON.parse(user);

        //+ userData.id

        var apiUrl = "http://localhost:8000/api" + window.location.pathname;

        console.log('apiUrl', apiUrl);
        //console.log('href', window.location.pathname);

        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };

        fetch(apiUrl, {
                method: 'GET',
                headers: headers,
            })
            .then(response => {
                return response.json();
            })
            .then(musicData => {
                
                console.log('allMusics', musicData.allMusics);
                var allMusics = musicData.allMusics;


                var tableBody = $('#musicListBody');

                var artistTitleEl = $("#artistTitle");
                //row = tr_clone;
                allMusics.forEach(function(item,key) {
                    var row = $("<tr></tr>");
                    
                    if(item.gender == 'm'){
                        gender = 'Male';
                    }else if(item.gender == 'f'){
                        gender = 'Female';
                    }else{
                        gender = 'Others';
                    }

                    row.append("<td class='sn'>" + (key + 1) + "</td>");
                    row.append("<td class='title'>" + item.title + "</td>");
                    row.append("<td class='album'>" + item.album_name + "</td>");
                    row.append("<td class='genre'>" + item.genre + "</td>");

                    var actionCell = $("<td class='action'></td>");
                    var musicListUrl = '/home/artist/' + item.id + '/musicList' ;
                    var editMusicUrl = '/home/music/editMusic/' + item.music_id ;
                    var deleteMusicUrl = '/home/artist/deleteMusic/' + item.music_id ;

                    var artistTitle = 'Music List of ' + item.name ;
                    console.log('artistTitle',artistTitle);

                    console.log('musicListUrl',musicListUrl);
                    artistTitleEl.html(artistTitle);
                    actionCell.append("<a class='btn btn-primary btn-md changeStatus' href="+ editMusicUrl +">Edit</a>");
                    actionCell.append("<a class='btn btn-danger btn-md changeStatus deleteUrl' href='#' onClick='deleteMusic("+item.music_id+",`"+item.title+"`)'>Delete</a>");
                    
                    
                    row.append(actionCell);
                    tableBody.append(row);
                });
            });
        
        // Make a DELETE request to send form data

        function deleteMusic(musicId,musicName){
            var deleteApiEndpoint = "http://localhost:8000/api/home/music/deleteMusic/" + musicId;
            console.log('delete>>',musicId);
            var userConfirmed =  confirm("Are you sure you want to delete music : `"+ musicName+"` ?");

            if(userConfirmed){
                fetch(deleteApiEndpoint, {
                    method: 'DELETE',
                    headers: headers
                })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json(); // Assuming you expect a JSON response
                })
                .then(data => {
                    // Handle the response data here
                    console.log('deleted>>',data);
                    if(data.success){
                        notify('success',data.success);

                        setTimeout(function(){
                            location.reload();
                        },3000);

                    }
                    // Optionally, you can update the UI to reflect the successful delete
                    // For example, you can remove the item from the page.
                })
                .catch(error => {
                    // Handle errors here
                    console.error('There was a problem with the fetch operation:', error);

                    // Optionally, you can display an error message or handle the error in other ways.
                });
            }
            
        }
    </script>
    @endsection