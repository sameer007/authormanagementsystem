@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            @include('layouts.notify')
            <div class="row mt">
                <div class="col-lg-12">
                    @php
                    $sessionData = session()->all();


                    @endphp
                    <table class="table table-striped table-advance table-hover">

                        <h4><i class="fa fa-angle-right"></i> <span id="artistTitle">Music List</span> 
                        </h4>

                        <hr />
                        <thead>
                            <tr>
                                <th><i class="fa fa-bookmark"></i> S.N</th>
                                <th><i class="fa fa-bullhorn"></i>Title</th>
                                <th class="hidden-phone"><i class="fa fa-question-circle"></i> Album Name</th>
                                <th><i class="fa fa-bookmark"></i> Genre</th>
                                <th><i class="fa fa-edit"></i> Action</th>
                            </tr>
                        </thead>
                        <tbody id="musicListBody">

                        </tbody>
                    </table>
                    <div class="dataTables_paginate paging_bootstrap pagination">
                        <ul id="paginationUl">
                            <li class="prev disabled" id="prev"><a href="#">← Previous</a></li>
                            <!-- <li class="active"><a href="#">1</a></li>
                            <li"><a href="#">2</a></li> -->

                            <li class="next disabled" id="next"><a href="#">Next → </a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </section>
        <!-- /wrapper -->
    </section>

    
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', token);
        console.log('user', user);
        const userData = JSON.parse(user);

        //+ userData.id
        const urlParams = new URLSearchParams(window.location.search);

        // Access individual query parameters
        const param2 = urlParams.get('param2');

        if (urlParams.has('page')) {
            // 'param1' exists in the URL
            var pageParam = urlParams.get('page'); // Gets the value of 'param1'
            var param = '?page='+ pageParam ;
        } else {
            var param = '' ;
        }
        var apiUrl = "http://localhost:8000/api" + window.location.pathname + param;

        console.log('apiUrl', apiUrl);
        //console.log('href', window.location.pathname);

        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };

        fetch(apiUrl, {
                method: 'GET',
                headers: headers,
            })
            .then(response => {
                return response.json();
            })
            .then(musicData => {
                console.log(musicData.success);
                //var allArtists = JSON.stringify(musicData.allArtists);
                console.log('musicData>>',musicData);

                var allMusics = musicData.allMusics.data;
                var firstPageUrl = musicData.allMusics.first_age_url ;
                var prevPageUrl = musicData.allMusics.prev_page_url ;

                var nextPageUrl = musicData.allMusics.next_page_url ;
                var lastPageUrl = musicData.allMusics.last_page_url ;
                var pageUrlLinks = musicData.allMusics.links ;

                var paginationUl = $('#paginationUl');
                var nextEl = $('#next');

                pageUrlLinks.forEach(function(linkItem,linkKey){
                    
                    if((linkKey != 0) && (linkKey != (pageUrlLinks.length - 1))){
                        var linkUrl = linkItem.url.replace("/api", "").replace('8000', '8080');

                        if(linkItem.label == pageParam){
                            var centerPageLink = $("<li class='active'><a href=" + linkUrl + ">" + linkItem.label + "</a></li>");

                        }else{
                            var centerPageLink = $("<li><a href=" + linkUrl + ">" + linkItem.label + "</a></li>");

                        }
                        //paginationUl.append(centerPageLink);
                        centerPageLink.insertBefore(nextEl);
                    }else{
                        // if(linkKey == 0){
                        //     $('#prev a').attr('href',linkUrl);
                        // }else if(linkKey == (pageUrlLinks.length - 1)){
                        //     $('#next a').attr('href',nextPageUrl);

                        // }
                    }

                    console.log('linkItem>>',linkItem);
                });
                
                
                console.log('allMusics', allMusics);
                
                console.log('prevPageUrl', prevPageUrl);
                console.log('nextPageUrl', nextPageUrl);
                if(prevPageUrl){
                    console.log('prevPageUrl', prevPageUrl);
                    
                    var lastPageLinkUrl = prevPageUrl.replace("/api", "").replace('8000', '8080');
                    $('#prev a').attr('href',lastPageLinkUrl);
                }
                if(nextPageUrl){
                    console.log('nextPageUrl', nextPageUrl);

                    var nextPageLinkUrl = nextPageUrl.replace("/api", "").replace('8000', '8080');
                    $('#next a').attr('href',nextPageLinkUrl);
                }
                var tableBody = $('#musicListBody');

                var artistTitleEl = $("#artistTitle");
                //row = tr_clone;
                allMusics.forEach(function(item,key) {
                    var row = $("<tr></tr>");
                    
                    if(item.gender == 'm'){
                        gender = 'Male';
                    }else if(item.gender == 'f'){
                        gender = 'Female';
                    }else{
                        gender = 'Others';
                    }

                    row.append("<td class='sn'>" + (key + 1) + "</td>");
                    row.append("<td class='title'>" + item.title + "</td>");
                    row.append("<td class='album'>" + item.album_name + "</td>");
                    row.append("<td class='genre'>" + item.genre + "</td>");

                    var actionCell = $("<td class='action'></td>");
                    var musicListUrl = '/home/artist/' + item.id + '/musicList' ;
                    var editMusicUrl = '/home/music/editMusic/' + item.id ;
                    var deleteMusicUrl = '/home/music/deleteMusic/' + item.id ;

                    
                    actionCell.append("<a class='btn btn-primary btn-md changeStatus' href="+ editMusicUrl +">Edit</a>");
                    actionCell.append("<a class='btn btn-danger btn-md changeStatus deleteUrl' href='#' onClick='deleteMusic("+item.id+",`"+item.title+"`)'>Delete</a>");
                    
                    
                    row.append(actionCell);
                    tableBody.append(row);
                });
                //console.log(row[0])
                


            });
        // Make a DELETE request to send form data

        function deleteMusic(musicId,musicName){
            var deleteApiEndpoint = "http://localhost:8000/api/home/music/deleteMusic/" + musicId;
            console.log('delete>>',musicId);
            var userConfirmed =  confirm("Are you sure you want to delete music : `"+ musicName+"` ?");

            if(userConfirmed){
                fetch(deleteApiEndpoint, {
                    method: 'DELETE',
                    headers: headers
                })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json(); // Assuming you expect a JSON response
                })
                .then(data => {
                    // Handle the response data here
                    console.log('deleted>>',data);
                    if(data.success){
                        notify('success',data.success);

                        setTimeout(function(){
                            location.reload();
                        },3000);

                    }
                    // Optionally, you can update the UI to reflect the successful delete
                    // For example, you can remove the item from the page.
                })
                .catch(error => {
                    // Handle errors here
                    console.error('There was a problem with the fetch operation:', error);

                    // Optionally, you can display an error message or handle the error in other ways.
                });
            }
            
        }
    </script>
    @endsection