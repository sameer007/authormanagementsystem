@extends('layouts.app')
<!-- js placed at the end of the document so the pages load faster -->
<script src="{{url('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{url('lib/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
    <script src="{{url('lib/jquery.ui.touch-punch.min.js')}}"></script>
    <script class="include" type="text/javascript" src="{{url('lib/jquery.dcjqaccordion.2.7.js')}}"></script>
    <script src="{{url('lib/jquery.scrollTo.min.js')}}"></script>
    <script src="{{url('lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <!--common script for all pages-->
    <script src="{{url('lib/common-scripts.js')}}"></script>
    <!--script for this page-->
@section('content')
<div id="login-page">
  <div class="container">
    <form class="form-login" id="myForm" action="/auth/register" method="post">

      <h2 class="form-login-heading">Register now</h2>
      @include('layouts.notify')
      <div class="login-wrap">
        <input type="text" name="first_name" class="form-control" placeholder="First name">
        <br>
        <input type="text" name="last_name" class="form-control" placeholder="Last name">
        <br>
        <input type="email" name="email" class="form-control" placeholder="User Email" autofocus>
        <br>
        <input type="number" name="phone" class="form-control" placeholder="Ph No">
        
        <div class="radio">
          Gender:
          <label>
            <input type="radio" name="gender" id="gender1" value="m" checked>
            Male
          </label>
          <label>
            <input type="radio" name="gender" id="gender2" value="f">
            Female
          </label>
          <label>
            <input type="radio" name="gender" id="gender3" value="f">
            Other
          </label>
        </div>


        <input type="date" name="dob" class="form-control" placeholder="DOB" onfocus="(this.type='date')" onblur="(this.type='text')" autofocus>
        <br>
        <input type="address" name="address" class="form-control" placeholder="Address">
        <br>
        <input type="password" name="password" class="form-control" placeholder="Password">
        <br>
        <input type="password" name="repassword" class="form-control" placeholder="Re-type Password">
        <br>
        <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
        <hr>
        <div class="registration">
          Login instead?<br />
          <a class="" href="{{url('/auth/login')}}">
            Login
          </a>
        </div>
      </div>
      <!-- Modal -->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Forgot Password ?</h4>
            </div>
            <div class="modal-body">
              <p>Enter your e-mail address below to reset your password.</p>
              <input type="text" name="resetPwdEmail" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
              <button class="btn btn-theme" type="button">Submit</button>
            </div>
          </div>
        </div>
      </div>
      <!-- modal -->
    </form>
  </div>
</div>
@endsection
@section('customJs')
<!-- js placed at the end of the document so the pages load faster -->
<script src="{{url('lib/jquery/jquery.min.js')}}"></script>
<script src="{{url('lib/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('lib/jquery-ui-1.9.2.custom.min.js')}}"></script>
<script src="{{url('lib/jquery.ui.touch-punch.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{url('lib/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{url('lib/jquery.scrollTo.min.js')}}"></script>
<script src="{{url('lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
<!--common script for all pages-->
<script src="{{url('lib/common-scripts.js')}}"></script>
<!--script for this page-->
<script type="text/javascript" src="{{url('lib/jquery.backstretch.min.js')}}"></script>
<script>
  $.backstretch("{{url('img/login-bg.jpg')}}", {
    speed: 500
  });
</script>
<script>
  document.getElementById("myForm").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent the default form submission behavior

    // Get form data
    const formData = new FormData(event.target);

    // Define the API endpoint
    const apiUrl = "http://localhost:8000/api/auth/register";

    // Make a POST request to send form data

    fetch(apiUrl, {
        method: "POST",
        body: formData,
      })
      .then((response) => response.json())
      .then((data) => {
        // Handle the API response (e.g., show a success message)
        //var userData = JSON.parse(data);
        console.log('registerData', data);
        if (data.success) {

          const userData = JSON.stringify(data.user);
          //console.log('token', data.token);
          console.log('user', userData);
          console.log('success', data.success);
          //localStorage.setItem('token', data.token);
          localStorage.setItem('success', data.success);
          //localStorage.setItem('user', userData);
          notify('success', data.success);

        }else if (data.error) {
          notify('error', data.error);
        }else{
          window.location.href = '/auth/register';
        }
      })
      .catch((error) => {
        // Handle errors (e.g., show an error message)
        console.error("Error:", error);
      });
  });
</script>
@endsection