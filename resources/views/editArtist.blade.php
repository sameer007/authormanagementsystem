@extends('layouts.app')

@section('content')
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    @include('layouts.topNavbar')
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    @include('layouts.sidebar')

    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Artist</h3>
            @include('layouts.notify')
            <!-- BASIC FORM ELELEMNTS -->

            @php
            @endphp
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><i class="fa fa-angle-right"></i> <span id="title"></span></h4>
                        <form id="editArtistForm" action="/home/artist/editArtist" METHOD="PUT">

                            <div class="box-body"></div>
                            <div class="form-group col-md-6">
                                <label>Artist Name</label>
                                <small class="req"> *</small>
                                <input class="form-control" id="artistName" autofocus="" name="name" placeholder="" type="text" value="" autocomplete="off" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>DOB</label>
                                <small class="req"> *</small>
                                <input type="date" id="dob" name="dob" class="form-control" placeholder="DOB" autofocus>
                                <span class="text-danger"></span>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-6">
                                <div class="radio">
                                    <label>Gender:</label>

                                    <label>
                                        <input type="radio" name="gender" id="genderm" value="m">
                                        Male
                                    </label>
                                    <label>
                                        <input type="radio" name="gender" id="genderf" value="f">
                                        Female
                                    </label>
                                    <label>
                                        <input type="radio" name="gender" id="gendero" value="o">
                                        Other
                                    </label>
                                </div>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input class="form-control" id="address" name="address" placeholder="" type="text" value="" />
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>First Release Year</label>
                                <small class="req"> *</small>
                                <select class="form-control" name="first_release_year" id="firstReleaseYear">
                                    <option value=""></option>
                                </select>
                                <span class="text-danger"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>No of Albums Relased</label>
                                <small class="req"> *</small>
                                <input type="number" id="noOfAlbumsReleased" name="no_of_albums_released" class="form-control" placeholder="No Of Albums Released">

                                <span class="text-danger"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="update-artist">Update Artist</button></div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!-- col-lg-12-->
            </div>
        </section>
        <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    @include('layouts.footer')
    <script>
        let startYear = 1800;
        let endYear = new Date().getFullYear();
        for (i = endYear; i > startYear; i--) {
            $('#firstReleaseYear').append($('<option/>').val(i).html(i));
        }
    </script>
    <script>
        // Define the API endpoint
        var token = localStorage.getItem('token').toString();
        var bearerToken = 'Bearer ' + token;
        var user = localStorage.getItem('user');

        console.log('token', bearerToken);
        console.log('user', user);
        const userData = JSON.parse(user);


        //+ userData.id
        var currentUrlArr = window.location.pathname.split("/");

        var artistIdFromPath = currentUrlArr[currentUrlArr.length - 1];
        var apiUrl = "http://localhost:8000/api/home/artist/singleArtist/" + artistIdFromPath;

        console.log('apiUrl', apiUrl);


        const headers = {
            'Content-Type': 'application/json', // Example content type
            'Authorization': bearerToken, // Example authorization header
        };
        let genderElId;
        fetch(apiUrl, {
                method: 'GET',
                headers: headers,
            })
            .then(response => {
                return response.json();
            })
            .then(data => {
                
                console.log('artistData', data.artistData);
                var artistData = data.artistData;

                console.log(artistData);
                genderElId = '#gender' + artistData.gender;

                var artistName = $('#artistName');
                var dob = $('#dob');
                var gender = $(genderElId);
                var address = $('#address');
                var firstReleaseYear = $('#firstReleaseYear');
                var noOfAlbumsReleased = $('#noOfAlbumsReleased');

                $("#title").html('Edit Artist Info of ' + artistData.name);

                var dateDob = artistData.dob.split(' ')[0];

                artistName.val(artistData.name);
                dob.val(dateDob);
                gender.attr('checked', 'checked');
                address.val(artistData.address);
                firstReleaseYear.val(artistData.first_release_year);
                noOfAlbumsReleased.val(artistData.no_of_albums_released);
                
            });
        // Make a PUT request to send form data

        document.getElementById("editArtistForm").addEventListener("submit", function(event) {
            event.preventDefault(); // Prevent the default form submission behavior

            // Get form data
            //const formData = new FormData(event.target);
            //const formData = new FormData(this);
            var artistName = $('#artistName');
            var dob = $('#dob');
            var gender = $(genderElId);
            var address = $('#address');
            var firstReleaseYear = $('#firstReleaseYear');
            var noOfAlbumsReleased = $('#noOfAlbumsReleased');
            console.log(dob.val());

            const formData = {
                'name': artistName.val(),
                'dob': dob.val(),
                'gender': gender.val(),
                'address': address.val(),
                'first_release_year': firstReleaseYear.val(),
                'no_of_albums_released': Number(noOfAlbumsReleased.val()),
            }
            console.log('formData>>', formData);

            // Make a PUT request to send form data

            var updateArtistApiUrl = "http://localhost:8000/api/home/artist/editArtist/1"
            
            console.log('updateArtistApiUrl', updateArtistApiUrl);
            console.log('headers', headers);

            fetch(updateArtistApiUrl, {
                    method: "PUT",
                    body: JSON.stringify(formData),
                    headers: headers,
                })
                .then((response) => response.json())
                .then((data) => {
                    // Handle the API response (e.g., show a success message)
                    console.log('updatedArtistData>>', data.updatedArtistData);

                    if (data.updatedArtistData[0]) {
                        var updatedArtistData = data.updatedArtistData[0];
                        $("#title").html('Edit Artist Info of ' + updatedArtistData.name);
                        if (data.success) {
                            notify('success', data.success);
                        } else if (data.error) {
                            notify('error', data.error);
                        }
                    }

                })
                .catch((error) => {
                    // Handle errors (e.g., show an error message)
                    console.error("Error:", error);
                });
        });
    </script>
    <script>

    </script>
    @endsection