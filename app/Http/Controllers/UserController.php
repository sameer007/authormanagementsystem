<?php

namespace App\Http\Controllers;

use App\Enums\Gender;
use App\Enums\Role;
use App\Models\Music;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //
    public function addUser(Request $request)
    {
        try {
            $user = new User();

            $validator = Validator::make($request->all(), $user->getValidationRules);

            if ($validator->fails()) {
                $validationErrMsg = json_encode($validator->messages());
                
                return response()->json([
                    'error' => 'Please insert valid user data'
                ], 403);
            } else {

                $user = $user->createUser($request, Role::User);

                if ($user) {

                    $token = $user->createToken('x-access-token')->plainTextToken;

                    //$token = Auth::login($user);

                    return response()->json([
                        'success' => 'New User : "' . $user->email . '" registered successfully',
                        'user' => $user,
                        'token' => $token
                    ], 200);
                } else {
                    return response()->json([
                        'error' => 'Something went wrong while registering user',
                    ], 500);
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function getLoggedInUserData($id)
    {
        try {
            $user = new User();
            $loggedinUserData = $user->getUserByIdAndRole((int)$id, Role::Admin)->first();
            //dd($loggedinUserData);
            if (isset($loggedinUserData) && !empty($loggedinUserData)) {

                return response()->json([
                    'success' => 'Logged in successfully as ' . $loggedinUserData->first_name,
                    'user' => $loggedinUserData
                ], 200);
            } else {
                return response()->json([
                    'error' => 'User not found',
                ], 403);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function editUser(Request $request, $id)
    {
        try {
            $user = new User();
            //dd($user);
            $validator = Validator::make($request->all(), $user->getEditUserValidationRules);
            //dd($validator->messages());
            $validationErrMsg = json_encode($validator->messages());

            if ($validator->fails()) {

                return response()->json([
                    'error' => 'Please insert valid User data'
                ], 403);
            } else {
                //dd($book_id);

                if (isset($id) && !empty($id)) {
                    $userData = $user->getUserById($id);


                    $dataToUpdate = array(
                        'first_name' => $request->input('first_name'),
                        'last_name' => $request->input('last_name'),
                        'phone' => $request->input('phone'),
                        'gender' => $request->input('gender'),
                        'dob' => $request->input('dob'),
                        'address' => $request->input('address')
                    );

                    if ($userData->isNotEmpty()) {


                        //dd('$dataToUpdate',$dataToUpdate);

                        $is_updated = $user->updateUser($dataToUpdate, $id);
                        //dd($is_updated);
                        if ($is_updated) {
                            //dd($profile_id,$updatedProfileId);

                            $updatedUserData = $user->getUserById($id);

                            //$resData = array('userData' => $updatedUserData);
                            //dd($updatedUserData);

                            return response()->json([
                                'success' => 'User info of : `' . $updatedUserData[0]->first_name . '` updated successfully',
                                'updatedUserData' => $updatedUserData
                            ], 200);
                        } else {
                            //dd($userData);

                            return response()->json([
                                'error' => 'Something went wrong while updating User info'
                            ], 500);
                        }
                    } else {
                        return response()->json([
                            'info' => 'User info not found in database'
                        ], 404);
                    }
                } else {
                    return response()->json([
                        'error' => 'Something went wrong while updating book info'
                    ], 404);
                }
                //dd('$validator success');
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function getUserList()
    {
        try {
            //dd($request->all());
            $user = new User();
            //dd($user);
            $allUsers = $user->getAllUserByRole(Role::User);
            
            return response()->json([
                'allUsers' => $allUsers
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }
    public function getUserById($id)
    {
        try {
            //dd($request->all());
            $user = new User();
            $userData = $user->getUserById($id);

            //dd($userData);
            if (isset($userData[0]) && !empty($userData[0])) {
                return response()->json([
                    'UserData' => $userData[0]
                ], 200);
            } else {
                return response()->json([
                    'info' => 'User Data not found in database'
                ], 404);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }
    public function deleteUser($id)
    {
        try {
            $user = new User();
            if (isset($id) && !empty($id)) {
                $userData = $user->getUserById($id);

                if ($userData->isEmpty()) {
                    return response()->json([
                        'error' => 'User not found in database'
                    ], 404);
                } else {
                    //dd($bookData);
                    $is_deleted = $user->deleteUser($userData[0]->id);

                    if ($is_deleted) {
                        return response()->json([
                            'success' => 'User : `' . $userData[0]->first_name . '` deleted successfully',
                        ], 200);
                    } else {
                        //dd($userData);
                        return response()->json([
                            'error' => 'Something went wrong while deleting User : `' . $userData[0]->first_name . '`',
                        ], 500);
                    }
                }
            } else {
                return response()->json([
                    'error' => 'User not found in database',
                ], 404);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }
}
