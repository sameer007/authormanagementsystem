<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\ApiService;
use Doctrine\Common\Lexer\Token;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AuthWebController extends Controller
{
    //
    private $apiService;
    public function __construct(ApiService $csvService)
    {
        $this->apiService = $csvService;
    }

    public function register(Request $request)
    {


        $user = new User();

        $validator = Validator::make($request->all(), $user->getValidationRules);
        if ($validator->fails()) {
            //dd($validator->messages());

            return redirect('auth/register')->with([
                'error' => 'Please insert valid user data for registration'
            ]);
        } else {

            $apiRoute = '/auth/register';
            $postData = $request->all();

            $data = $this->apiService->apiPostRequest($apiRoute, $postData);


            if ($data) {
                $dataArray = (array)$data;
                //dd($dataArray);
                if (isset($dataArray) && !empty($dataArray)) {
                    //
                    //dd($dataArray['token'],$dataArray['success']);
                    if (isset($dataArray['success']) && !empty($dataArray['success'])) {
                        return redirect('auth/login')->with([
                            'success' => $dataArray['success']
                        ]);
                    } else {
                        
                        return redirect('auth/login')->with([
                            'error' => 'Something went wrong while registering user'
                        ]);
                    }
                } else {
                    return redirect('auth/login')->with([
                        'error' => 'Something went wrong while registering user'
                    ]);
                }
            } else {
                return redirect('auth/login')->with([
                    'error' => 'Something went wrong while logging in'
                ]);
            }
        }
    }

    public function login(Request $request)
    {
        try {
            $user = new User();

            $validator = Validator::make($request->all(), $user->getLoginValidationRules);
            //dd($validator);
            if ($validator->fails()) {

                $resData = [
                    'message' => 'Please insert valid user data for registration'
                ];
                return view('login', $resData);
            } else {
                //do api call post request
                $apiRoute = '/auth/login';

                $postData = $request->all();
                $data = $this->apiService->apiPostRequest($apiRoute, $postData);
                if ($data) {
                    $dataArray = (array)$data;

                    $token = 'Bearer ' . $data->token;
                    //dd($token);
                    $request->session()->put('token', $token);
                    $request->session()->put('user', $dataArray['user']);

                    // session([
                    //     'token' => $token,
                    //     'user' => $data
                    // ]);

                    // $resData = [
                    //     'token' => $token
                    // ];
                    //array_push($resData,(array)$data);
                    return redirect('home/dashboard')->with([
                        'success' => $dataArray['success']
                    ]);
                } else {
                    return redirect('auth/login')->with([
                        'error' => 'Something went wrong while logging in'
                    ]);
                }


                //dd($validator->messages());

            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function logout(Request $request)
    {
        //$request->session()->invalidate();
        // $request->session()->regenerateToken();

        return redirect('auth/login')->with([
            'success' => 'Logged out successfully',
        ]);
    }

    public function showRegisterPage()
    {
        return view('index');
    }
    public function showLoginPage()
    {
        return view('login');
    }
}
