<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\ApiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DashboardWebController extends Controller
{
    //
    private $apiService;
    public function __construct(ApiService $csvService)
    {
        $this->apiService = $csvService;
    }
    public function showDashboard(Request $request)
    {
        // $token = $request->session()->get('token');
        // $user = $request->session()->get('user');

        // //dd($user);
        // $apiRoute = '/home/dashboard'.$user->id ;
        // $userData = $this->apiService->apiGetRequest($apiRoute,$token);
        // dd($userData);
        // $resData = array('userData'=>$userData);
        return view('dashboard');
        
    }
}
