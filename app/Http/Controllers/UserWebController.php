<?php

namespace App\Http\Controllers;

use App\Enums\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserWebController extends Controller
{
    //

    public function showUserList(){
        return view('userList');
    }
    public function showCreateNewUserPage(){
        return view('addUser');
    }
    public function showEditUserPage(){
        return view('editUser');
    }
}
