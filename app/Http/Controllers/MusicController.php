<?php

namespace App\Http\Controllers;

use App\Models\Music;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MusicController extends Controller
{
    //
    public function addNewMusic(Request $request, $artistId)
    {
        try {
            $music = new Music();
            //dd($music);
            $validator = Validator::make($request->all(), $music->getValidationRules);
            //dd($validator->messages());
            if ($validator->fails()) {
                $validationErrMsg = json_encode($validator->messages());

                return response()->json([
                    'error' => 'Please insert valid music data'
                ], 403);
            } else {

                $music_id = $music->addMusic($request, $artistId);

                if (isset($music_id) && !empty($music_id)) {

                    //dd((int)$music_id,'music id>>',$music_id);

                    $musicData = $music->getMusicAndArtistByMusicId((int)$music_id);
                    //dd($musicData);
                    return response()->json([
                        'success' => 'New Music : `' . $musicData[0]->title . '` added successfully to album `' . $musicData[0]->album_name.'`',
                        'Music' => $musicData[0]
                    ], 200);
                } else {
                    return response()->json([
                        'error' => 'Something went wrong while adding Music',
                    ], 500);
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function editMusicInfo(Request $request, $id)
    {
        try {
            $music = new Music();
            //dd($music);
            $validator = Validator::make($request->all(), $music->getValidationRules);

            if ($validator->fails()) {

                return response()->json([
                    'message' => 'Please insert valid Music data'
                ], 403);
            } else {
                //dd($book_id);

                if (isset($id) && !empty($id)) {
                    $musicData = $music->getMusicAndArtistByMusicId($id);


                    $dataToUpdate = array(
                        'title' => $request->input('title'),
                        'album_name' => $request->input('album_name'),
                        'genre' => $request->input('genre')
                    );

                    if ($musicData->isNotEmpty()) {


                        //dd('$dataToUpdate',$dataToUpdate);

                        $is_updated = $music->updateMusic($dataToUpdate, $id);

                        if ($is_updated) {
                            //dd($profile_id,$updatedProfileId);

                            $updatedMusicData = $music->getMusicAndArtistByMusicId($id);

                            //$resData = array('userData' => $updatedUserData);
                            //dd($updatedUserData);

                            return response()->json([
                                'success' => 'Music info of album : `' . $updatedMusicData[0]->album_name . '` updated successfully',
                                'updatedMusicData' => $updatedMusicData
                            ], 200);
                        } else {
                            //dd($userData);

                            return response()->json([
                                'error' => 'Something went wrong while updating Music info'
                            ], 500);
                        }
                    } else {
                        return response()->json([
                            'info' => 'Music info not found in database'
                        ], 404);
                    }
                } else {
                    return response()->json([
                        'error' => 'Something went wrong while updating music info'
                    ], 404);
                }
                //dd('$validator success');
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function getMusicList()
    {
        try {
            //dd($request->all());
            $music = new Music();
            //dd($music);
            $allMusics = $music->getAllMusic();


            return response()->json([
                'allMusics' => $allMusics
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function getMusicById($id)
    {
        try {
            //dd($request->all());
            $music = new Music();
            $musicData = $music->getMusicAndArtistByMusicId($id);

            //dd($musicData);
            if (isset($musicData[0]) && !empty($musicData[0])) {
                return response()->json([
                    'musicData' => $musicData[0]
                ], 200);
            } else {
                return response()->json([
                    'info' => 'Music Data not found in database'
                ], 404);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }
    public function deleteMusic($id)
    {
        try {
            $music = new Music();
            if (isset($id) && !empty($id)) {
                $musicData = $music->getMusicAndArtistByMusicId($id);

                if ($musicData->isEmpty()) {
                    return response()->json([
                        'error' => 'Music not found in database'
                    ], 404);
                } else {
                    //dd($bookData);
                    $is_deleted = $music->deleteMusic($musicData[0]->id);

                    if ($is_deleted) {
                        return response()->json([
                            'success' => 'Music : `' . $musicData[0]->title . '` deleted from album `'.$musicData[0]->album_name.'` successfully',
                        ], 200);
                    } else {
                        //dd($userData);
                        return response()->json([
                            'error' => 'Something went wrong while deleting Music from album : `' . $musicData[0]->album_name . '`',
                        ], 500);
                    }
                }
            } else {
                return response()->json([
                    'error' => 'Music not found in database',
                ], 404);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }
}
