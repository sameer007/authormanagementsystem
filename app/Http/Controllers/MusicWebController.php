<?php

namespace App\Http\Controllers;

use App\Models\Music;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MusicWebController extends Controller
{
    //
    public function showMusicListPage(Request $request){
        
        return view('musicList');
    }

    public function showAllMusicListPage(Request $request){
        
        return view('allMusicList');
    }

    public function showAddNewMusicPage(Request $request){
        
        return view('addMusic');
    }
    public function showEditMusicPage(Request $request){
        
        return view('editMusic');
    }
}
