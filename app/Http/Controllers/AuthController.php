<?php

namespace App\Http\Controllers;

use App\Enums\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //
    public function register(Request $request)
    {

        try{
            $user = new User();

            $validator = Validator::make($request->all(), $user->getValidationRules);
            //dd($validator->messages());
            if ($validator->fails()) {
                $validationErrMsg = json_encode($validator->messages());
                
                return response()->json([
                    'error' => 'Please insert valid user data for registration'
                ], 403);
            } else {
                
                $user = $user->createUser($request,Role::Admin);
                if ($user) {
                    //dd($user);
                    
                    //$token = Auth::login($user);
                    
                    return response()->json([
                        'success' => 'New User : "' . $user->email . '" registered successfully',
                        'user' => $user
                    ], 200);
                } else {
                    return response()->json([
                        'error' => 'Something went wrong while registering user',
                    ], 500);
                }
            } 
        }catch(Exception $e){
            return response()->json([
                'error' => 'Exception occured :'.$e->__toString()
            ], 403);
        }
        
    }

    public function login(Request $request)
    {
        try{
            $user = new User();

            $validator = Validator::make($request->all(), $user->getLoginValidationRules);
            //dd($validator);
            if ($validator->fails()) {
                return response()->json([
                    'error' => 'Please insert valid user data for login',
                ], 403);
            } else {
    
                if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                    
                    //get loggedinUserData
                    //$loggedinUserData = $user->getUserByEmailAndRole($request->email,Role::Admin)->first();
                    $user = Auth::user();
                    
                    //dd($token);
                    
                    if($user){
                        $user->tokens()->delete();
                        $token = $user->createToken('x-access-token')->plainTextToken;
                        
                        //dd($token);
                        return response()->json([
                            'success' => 'Logged in successfully as ' . $user->first_name,
                            'user' => $user,
                            'token' => $token
                        ], 200);
                    }else{
                        return response()->json([
                            'error' => 'User not found',
                        ], 403);  
                    }
                } else {
                    return response()->json([
                        'error' => 'Invalid token',
                    ], 403);
                }
            } 
        }catch(Exception $e){
            return response()->json([
                'error' => 'Exception occured :'.$e->__toString()
            ], 403);
        }
        
    }
}
