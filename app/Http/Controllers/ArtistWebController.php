<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Services\ApiService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArtistWebController extends Controller
{
    //
    private $apiService;
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }
    
    public function showArtistListPage(Request $request){
        
        return view('artistList');
    }

    public function showAddArtistPage(Request $request){
        
        return view('addArtist');
    }
    public function showEditArtistPage(Request $request){
        
        return view('editArtist');
    }

    public function showArtistMusicPage(Request $request){
        
        return view('artistMusic');
    }

    public function showImportArtistsPage(Request $request){
        
        return view('importArtist');
    }
}
