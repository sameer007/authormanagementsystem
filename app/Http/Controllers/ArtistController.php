<?php

namespace App\Http\Controllers;

use App\Imports\ArtistsImport;
use App\Models\Artist;
use App\Services\CSVService;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ArtistController extends Controller
{
    //
    private $csvService;
    public function __construct(CSVService $csvService)
    {
        $this->csvService = $csvService;
    }
    public function addArtistsFromCSV(Request $request)
    {

        try {
            $artist = new Artist();

            $validator = Validator::make($request->all(), $artist->getValidationRulesForCSVImport);
            $validationErrMsg = json_encode($validator->messages());
            if ($validator->fails()) {
                return response()->json([
                    'error' => 'Please insert valid CSV data'
                ], 403);
            } else {
                $csvFilePath = $request->file('artistsCSV');
                $this->csvService->importDataFromCSVToDatabase($csvFilePath);
                //$artistData = $this->csvService->readDataFromCSV($path) ;
                return response()->json([
                    'success' => 'Artist Data Imported successfully'
                ], 200);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured : '.$e->getMessage()
            ], 403);
        }
    }
    public function addNewArtist(Request $request)
    {
        try {
            $artist = new Artist();
            //dd($artist);
            $validator = Validator::make($request->all(), $artist->getValidationRules);
            //dd($validator->messages());
            $validationErrMsg = json_encode($validator->messages());

            if ($validator->fails()) {

                return response()->json([
                    'error' => 'Please insert valid artist data'
                ], 403);
            } else {

                $artist_id = $artist->addArtist($request);

                if (isset($artist_id) && !empty($artist_id)) {


                    $artistData = $artist->getArtistById((int)$artist_id);
                    //dd($artistData);
                    return response()->json([
                        'success' => 'New Artist : `' . $artistData[0]->name . '` added successfully',
                        'artist' => $artistData[0]
                    ], 200);
                } else {
                    return response()->json([
                        'error' => 'Something went wrong while adding artist',
                    ], 500);
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function editArtistInfo(Request $request, $id)
    {
        try {
            $artist = new Artist();
            //dd($artist);
            $validator = Validator::make($request->all(), $artist->getValidationRules);
            $validationErrMsg = json_encode($validator->messages());

            if ($validator->fails()) {

                return response()->json([
                    'message' => $validationErrMsg 
                ], 403);
            } else {
                //dd($book_id);

                if (isset($id) && !empty($id)) {
                    $artistData = $artist->getArtistById($id);


                    $dataToUpdate = array(
                        'name' => $request->input('name'),
                        'dob' => $request->input('dob'),
                        'gender' => $request->input('gender'),
                        'address' => $request->input('address'),
                        'first_release_year' => $request->input('first_release_year'),
                        'no_of_albums_released' => $request->input('no_of_albums_released')
                    );

                    if ($artistData->isNotEmpty()) {


                        //dd('$dataToUpdate',$dataToUpdate);

                        $is_updated = $artist->updateArtist($dataToUpdate, $id);

                        if ($is_updated) {
                            //dd($profile_id,$updatedProfileId);

                            $updatedArtistData = $artist->getArtistById($id);

                            //$resData = array('userData' => $updatedUserData);
                            //dd($updatedUserData);

                            return response()->json([
                                'success' => 'Artist info of : `' . $updatedArtistData[0]->name . '` updated successfully',
                                'updatedArtistData' => $updatedArtistData
                            ], 200);
                        } else {
                            //dd($userData);

                            return response()->json([
                                'error' => 'Something went wrong while updating artist info'
                            ], 500);
                        }
                    } else {
                        return response()->json([
                            'info' => 'Artist info not found in database'
                        ], 404);
                    }
                } else {
                    return response()->json([
                        'error' => 'Something went wrong while updating book info'
                    ], 404);
                }
                //dd('$validator success');
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function getArtistList()
    {
        try {
            //dd($request->all());
            $artist = new Artist();
            //dd($artist);
            $allArtists = $artist->getAllArtists();

            if(isset($allArtists)){
                return response()->json([
                    'allArtists' => $allArtists
                ], 200);
            }else{
                return response()->json([
                    'error' => 'Artists data not found'
                ], 403);
            }
            
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function getMusicListByArtistId(Request $request, $artistId)
    {
        try {
            //dd($request->all());
            $artist = new Artist();

            //dd($artistId);
            $allMusicsOfArtist = $artist->getMusicAndArtistByArtistId((int)$artistId);

            //dd($allMusicsOfArtist);
            return response()->json([
                'allMusics' => $allMusicsOfArtist
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }

    public function getArtistById($id)
    {
        try {
            //dd($request->all());
            $artist = new Artist();
            $artistData = $artist->getArtistById($id);

            //dd($artistData);
            if (isset($artistData[0]) && !empty($artistData[0])) {
                return response()->json([
                    'artistData' => $artistData[0]
                ], 200);
            } else {
                return response()->json([
                    'error' => 'Artist Data not found in database'
                ], 404);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }
    public function deleteArtist($id)
    {
        try {
            $artist = new Artist();
            if (isset($id) && !empty($id)) {
                $artistData = $artist->getArtistById($id);

                if ($artistData->isEmpty()) {
                    return response()->json([
                        'error' => 'Artist not found in database'
                    ], 404);
                } else {
                    //dd($bookData);
                    $is_deleted = $artist->deleteArtist($artistData[0]->id);

                    if ($is_deleted) {
                        return response()->json([
                            'success' => 'Artist : `' . $artistData[0]->name . '` deleted successfully',
                        ], 200);
                    } else {
                        //dd($userData);
                        return response()->json([
                            'error' => 'Something went wrong while deleting artist : `' . $artistData[0]->name . '`',
                        ], 500);
                    }
                }
            } else {
                return response()->json([
                    'error' => 'Artist not found in database',
                ], 404);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Exception occured :' . $e->__toString()
            ], 403);
        }
    }
}
