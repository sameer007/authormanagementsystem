<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class TokenRoleCheckMiddleware
{
    public function handle($request, Closure $next, $role)
    {
        if (Auth::check() && Auth::user()) {
            return $next($request);
        }

        return response()->json(['message' => 'Unauthorized'], 401);
    }
}