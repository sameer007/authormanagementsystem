<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Music extends Model
{
    use HasFactory;

    public $getValidationRules = [
        'title' => 'required|string|max:255',
        'album_name' => 'required|string|max:255',
        'genre' => 'required|in:rnb,country,classic,rock,jazz',

    ];

    public function addMusic($request,$artist_id)
    {
        $dataToinsert = array(
            'artist_id' => (int)$artist_id,
            'title' => $request->input('title'),
            'album_name' => $request->input('album_name'),
            'genre' => $request->input('genre')
        );

        $is_added = DB::table('music')
            ->insert($dataToinsert);

        
        if ($is_added) {
            $added_id = DB::getPdo()->lastInsertId();
            return $added_id;
        } else {
            return $is_added;
        }
    }

    public function getAllMusic()
    {
        $music = DB::table('music')
            ->paginate(3);
        return $music;
    }

    public function getMusicById($id)
    {
        $user = DB::table('music')
            ->where('id', $id)
            ->get();
        return $user;
    }

    public function getMusicAndArtistByMusicId($musicId)
    {
        $musicAndArtistData = DB::table('music')
            ->select('music.id','music.artist_id','artist.name','artist.gender','artist.address','artist.first_release_year',
            'artist.no_of_albums_released','music.title','music.album_name','music.genre')
            ->join('artist', 'music.artist_id', '=', 'artist.id')
            ->where('music.id', $musicId)
            ->get();
        return $musicAndArtistData;
    }
    
    public function updateMusic($dataToUpdate, $musicId, $is_die = false)
    {
        //dd($dataToUpdate,$musicId);
        
        $is_updated = DB::table('music')
            ->where('id', $musicId)
            ->update($dataToUpdate);

        if ($is_die) {
            dd(DB::getQueryLog());
        }

        return $is_updated;
    }

    public function deleteMusic($musicId, $is_die = false)
    {
        $is_deleted = DB::table('music')
            ->where('id', $musicId)
            ->delete();
            
        if ($is_die) {
            dd(DB::getQueryLog());
        }

        return $is_deleted;
    }
}
