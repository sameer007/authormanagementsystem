<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Artist extends Model
{
    use HasFactory;

    protected $table = 'artist';
    protected $fillable = ['name','dob','gender','address','first_release_year','no_of_albums_released'];

    public $getValidationRules = [
        'name' => 'required|string|max:255',
        'dob' => 'required|date|before:today',
        'gender' => 'required|in:m,f,o',
        'address' => 'required|string|max:255',
        'first_release_year' => 'required|date_format:Y',
        'no_of_albums_released' => 'required|integer|min:0',

    ];

    public $getValidationRulesForCSVFileData = [
        '*.name' => 'required|string|max:255',
        '*.dob' => 'required|date|before:today',
        '*.gender' => 'required|in:m,f,o',
        '*.address' => 'required|string|max:255',
        '*.first_release_year' => 'required|date_format:Y',
        '*.no_of_albums_released' => 'required|integer|min:0',
    ];

    public $getValidationRulesForCSVImport = [
        'artistsCSV' => 'file|mimes:csv'

    ];

    public function addArtist($request)
    {

        $dataToinsert = array(
            'name' => $request->input('name'),
            'dob' => $request->input('dob'),
            'gender' => $request->input('gender'),
            'address' => $request->input('address'),
            'first_release_year' => $request->input('first_release_year'),
            'no_of_albums_released' => $request->input('no_of_albums_released')
        );

        $is_added = DB::table('artist')
            ->insert($dataToinsert);

        
        if ($is_added) {
            $added_id = DB::getPdo()->lastInsertId();
            return $added_id;
        } else {
            return $is_added;
        }
        
        return $is_added;
    }

    public function getAllArtists()
    {
        $artist = DB::table('artist')
            ->paginate(3);
        return $artist;
    }

    public function getMusicAndArtistByArtistId($artistId)
    {
        
        $musicAndArtistData = DB::table('artist')
            ->select('music.id AS music_id','music.artist_id','artist.name','artist.gender','artist.address','artist.first_release_year',
            'artist.no_of_albums_released','music.title','music.album_name','music.genre')
            ->join('music', 'artist.id', '=', 'music.artist_id')
            ->where('music.artist_id', $artistId)
            ->get();
        
            //dd(DB::getQueryLog());    
        return $musicAndArtistData;
    }

    public function getArtistById($id)
    {
        $artist = DB::table('artist')
            ->where('id', $id)
            ->get();
        return $artist;
    }
    public function updateArtist($dataToUpdate, $artistId, $is_die = false)
    {
        //dd($dataToUpdate,$artistId);
        
        $is_updated = DB::table('artist')
            ->where('id', $artistId)
            ->update($dataToUpdate);

        if ($is_die) {
            dd(DB::getQueryLog());
        }

        return $is_updated;
    }
    public function deleteArtist($artistId, $is_die = false)
    {
        $is_deleted = DB::table('artist')
            ->where('id', $artistId)
            ->delete();
            
        if ($is_die) {
            dd(DB::getQueryLog());
        }

        return $is_deleted;
    }
}
