<?php

namespace App\Models;

use App\Enums\Gender;
use App\Enums\Role;

use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Enum;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public function __construct()
    {
        DB::enableQueryLog();
    }

    public $getValidationRules = [
        'first_name' => 'required|string|max:255',
        'last_name' => 'required|string|max:255',
        'email' => 'required|email|unique:users|max:255',
        'password' => 'required|string|max:500',
        'phone' => 'required|string|max:20',
        'gender' => 'required|string',
        'dob' => 'required|date|before:today',
        'address' => 'required|string|max:255'

    ];

    public $getEditUserValidationRules = [
        'first_name' => 'required|string|max:255',
        'last_name' => 'required|string|max:255',
        'phone' => 'required|string|max:20',
        'gender' => 'required|string',
        'dob' => 'required|date|before:today',
        'address' => 'required|string|max:255'

    ];

    public $getLoginValidationRules = [
        'email' => 'required|email|max:255',
        'password' => 'required|string|max:500',

    ];
    public function createUser($request,$role): self
    {
        $this->first_name = $request->input('first_name');
        $this->last_name = $request->input('last_name');
        $this->email = $request->input('email');
        $this->phone = $request->input('phone');
        $this->dob = $request->input('dob');
        $this->role = $role;
        $this->gender = $request->input('gender');
        $this->address = $request->input('address');
        $this->email = $request->input('email');
        $this->password = Hash::make($request->input('password'));

        $this->save();
        //User::create($this);
        return $this;
    }
    public function updateUser($dataToUpdate, $id, $is_die = false)
    {
        //dd($dataToUpdate,$id);
        $is_updated = DB::table('users')
            ->where('id', $id)
            ->update($dataToUpdate);

        if ($is_die) {
            dd(DB::getQueryLog());
        }

        return $is_updated;
    }
    public function getAllUsers()
    {
        $users = DB::table('users')
            ->get();
        return $users;
    }
    public function getUserByEmailAndRole($email,$role)
    {
        $user = DB::table('users')
            ->where('email', $email)
            ->where('role', $role)
            ->get();
        return $user;
    }

    public function getUserById($id)
    {
        $user = DB::table('users')
            ->where('id', $id)
            ->get();
        return $user;
    }

    public function getUserByIdAndRole($id,$role)
    {
        $user = DB::table('users')
            ->where('id', $id)
            ->where('role', $role)
            ->get();
        return $user;
    }

    public function getAllUserByRole($role)
    {
        $user = DB::table('users')
            ->where('role', $role)
            ->paginate(3);
        return $user;
    }

    public function getAllUser($role)
    {
        $user = DB::table('users')
            ->where('role', $role)
            ->get();
        return $user;
    }

    public function getAllUserDataByEmail($email, $is_die = false)
    {
        $users = DB::table('users')
            ->select('users.id', 'users.name', 'users.email', 'profile.image', 'profile.personal_email', 'profile.dob', 'profile.gender', 'users.created_at', 'users.updated_at')
            ->join('profile', 'users.id', '=', 'profile.user_id')
            ->where('users.email', $email)
            ->get();

        if ($is_die) {
            dd(DB::getQueryLog());
        }
        return $users;
    }

    public function deleteUser($userId, $is_die = false)
    {
        $is_deleted = DB::table('users')
            ->where('id', $userId)
            ->delete();
            
        if ($is_die) {
            dd(DB::getQueryLog());
        }

        return $is_deleted;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
