<?php

use Illuminate\Support\Facades\DB;

if(!function_exists('debugger')){
  function debugger($data, $is_die = false){
    echo "<pre style='color: #FF0000;'>";
    echo print_r($data);
    echo "</pre>";
    if($is_die){
      exit;
    }
  }
}

if(!function_exists('getCurrentPage')){
  function getCurrentPage(){
    $currentPage = substr(strrchr(url()->current(),"/"),1);
    debugger($currentPage);
    $currentPage = preg_replace('/\d+/u', '', $currentPage);
    return $currentPage;
  }
}

if(!function_exists('getCurrentRoute')){
  function getCurrentRoute(){
    $reqSegment = collect(request()->segments())->last();
    $currentRoute = preg_replace('/\d/', '', $reqSegment);  
    return $currentRoute;
  }
}