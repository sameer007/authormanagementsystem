<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Http;

class ApiService
{
    protected array $headers;
    protected string $apiUrl ;

    public function apiGetRequest($apiRoute,$token = null,$uploadFiles = false)
    {
        
        $this->headers = $this->setApiHeaderForGetReq($token);
        //dd($apiUrl,$this->headers);
        $this->apiUrl = env('API_URL').$apiRoute;

        //dd($this->apiUrl,$this->headers);
        $response = Http::withHeaders($this->headers)->get($this->apiUrl);

        //dd($response->body());
        //return apiResponseData if status code is 200
        if ($response->status() == '200') {

            $apiResponseData = json_decode($response->body());
            
            //dd($apiResponseData);

            return $apiResponseData;
        } else {

            return false;
        }
    }

    public function apiPostRequest($apiRoute,$postData,$token = null,$uploadFiles = false)
    {
        $this->headers = $this->setApiHeaderForPostReq($uploadFiles,$token);

        $this->apiUrl = env('API_URL').$apiRoute;
        
        //dd($this->headers,$postData);
        $response = Http::withHeaders($this->headers)->post($this->apiUrl,$postData);

        //dd($response->body());
        //return historical data if status code is 200
        if ($response->status() == '200') {
            $apiResponseData = json_decode($response->body());
            
            //dd($filtered_historicalData);

            return $apiResponseData;
        } else {

            return false;
        }
    }

    private function setApiHeaderForPostReq($uploadFiles,$token){
        $headerArray = array();
        $header = array();
        if(isset($token) && !empty($token)){
            $header['Authorization'] = $token;

        }
        if($uploadFiles){
            $header['Content-Type'] = 'multipart/form-data';
        }else{
            $header['Content-Type'] = 'application/x-www-form-urlencoded';
        }
        array_push($headerArray,$header);
        return $headerArray ;
    }

    private function setApiHeaderForGetReq($token){
        $headerArray = array();
        $header = array();
        if(isset($token) && !empty($token)){
            $header['Authorization'] = $token;

        }
        
        $header['Content-Type'] = 'application/json';
        $header['Accept'] = '*/*';
        
        array_push($headerArray,$header);
        return $headerArray ;
    }
    /**
     * function to retreive historical data using API call
     * 
     * */
    public function getHistoricalData(array $companyData)
    {
        $this->headers = [
            'X-RapidAPI-Key' => env('API_KEY'),
            'X-RapidAPI-Host' => env('API_HOST'),
        ];
        try {
            $companySymbol = $companyData['symbol'];

            $apiUrl = 'https://yh-finance.p.rapidapi.com/stock/v3/get-historical-data?symbol=' . $companySymbol . '&region=US';

            //historical data API call

            $response = Http::withHeaders($this->headers)->get($apiUrl);

            //return historical data if status code is 200
            if ($response->status() == '200') {
                $apiResponseData = json_decode($response->body());
                $historicalData = $apiResponseData->prices;

                //converting dates to millisecond for comparision
                $startDateMillisecond = strtotime($companyData['startDate']) * 1000;
                $endDateMillisecond = strtotime($companyData['endDate']) * 1000;

                //filter retreived historical data by startDate and endDate
                $filtered_historicalData = array_filter($historicalData, function ($item) use ($startDateMillisecond, $endDateMillisecond) {
                    return $item->date >= $startDateMillisecond && $item->date <= $endDateMillisecond;
                });
                //dd($filtered_historicalData);

                return $filtered_historicalData;
            } else {

                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * function to retreive historical data using API call
     * 
     * */
    public function validateSymbol($request)
    {

        try {

            $nasdaqListedCompanyApiUrl  = 'https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json';

            //historical data API call

            $response = Http::get($nasdaqListedCompanyApiUrl);
            //dd($response);
            //return historical data if status code is 200
            if ($response->status() == '200') {
                $apiResponseData = json_decode($response->body());
                $index = array_search($request->symbol, array_column($apiResponseData, 'Symbol'));

                if ($index !== false) {
                    $validatedCompanyData = array(
                        "symbol" => $request->symbol,
                        "startDate" => $request->startDate,
                        "endDate" => $request->endDate,
                        "email" => $request->email,
                        "companyInfoWithSymbol" => $apiResponseData[$index]
                    );
                } else {
                    $validatedCompanyData = false;
                }
                return $validatedCompanyData;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
}
