<?php

namespace App\Services;

use App\Imports\ArtistsImport;
use App\Mail\SendChartInfoEmail;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class CSVService
{
    public function importDataFromCSVToDatabase($csvFilePath)
    {
        Excel::import(new ArtistsImport, $csvFilePath);
        
    }
}