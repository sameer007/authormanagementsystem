<?php
namespace App\Enums;

 

enum Role:string {

    case Admin = 'Admin';
    case User = 'User';

}