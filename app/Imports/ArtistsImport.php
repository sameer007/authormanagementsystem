<?php

namespace App\Imports;

use App\Models\Artist;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Validators\Failure;

class ArtistsImport implements ToCollection,WithHeadingRow,WithBatchInserts,WithChunkReading
{
    public function collection(Collection $rows)
    {
        $artist = new Artist();
        $validator = Validator::make($rows->toArray(), $artist->getValidationRulesForCSVFileData);
        $validator->validate();

        foreach ($rows as $row) {
            $artist->create([
                'name' => $row['name'],
                'dob' => $row['dob'],
                'gender' => $row['gender'],
                'address' => $row['address'],
                'first_release_year' => (int)$row['first_release_year'],
                'no_of_albums_released' => $row['no_of_albums_released']
            ]);
        }
    }

    public function batchSize(): int
    {
        return 1000;
    }
    
    public function chunkSize(): int
    {
        return 1000;
    }
}
